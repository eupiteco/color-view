#!/bin/bash

### Funções / Functions
_getRatio () {
	if [ $1 -gt $2 ]
	then
		echo $(($1 - $2))
	else
		echo $(($2 - $1))
	fi
}
_getRed () {
	echo $1 | cut -c 2,3
}
_getGreen () {
	echo $1 | cut -c 4,5
}
_getBlue () {
	echo $1 | cut -c 6,7
}

_getDecimal () {
	echo $((16#$1))
}

_getTotal () {
	echo $(($1 + $2 + $3))
}

_padNumber () {
	printf "%03d\n" $1
}

### Chaves

sortColors=0
separate=0
showInfo=0
error=0
errorMsg=""

maxColors=0

if [ -z "$1" ] 
then
	error=1
	errorMsg="Faltando argummento"
fi

### Loop das opções, sempre q tiver uma penúltima, vai ser uma chave
while [ ! -z $2 ]
do
	case $1 in
		-s | --sort)
			sortColors=1
			shift
			;;
		-i | --info)
			showInfo=1
			shift
			;;
		--separate)
			separate=1
			threshold=40
			shift
			;;
		*)
			error=1
			errorMsg="Argumento inválido"
			break
			;;
	esac

done

if [ "$error" -eq 1 ]
then
	echo -n "ERRO! "
	echo "$errorMsg"
	exit 2
fi

## Declarando variáveis
colorList="$1"
outFile="sorted-$colorList"
tempFile="tmp.txt"

html1="templates/html1.txt"
html2="templates/html2.txt"
html3="templates/html3.txt"
content="content.txt"
outHtml="sorted-color-list.html"
if [ "$sortColors" -eq 1 -o "$separate" -eq 1 ]
then
	fileToHtml="$outFile"
else
	fileToHtml="$colorList"
fi

if [ "$separate" -eq 1 ]
then
	doneColor=0
	blackFile="black.txt"
	rFile="red.txt"
	gFile="green.txt"
	bFile="blue.txt"
	yFile="yellow.txt"
	mFile="magenta.txt"
	cFile="cyan.txt"
	grayFile="gray.txt"
fi

### Loop principal / Main loop
if [ "$sortColors" -eq 1 -o "$separate" -eq 1 ]
then
	i=1
	echo "Ordenando cores"
	while read color
	do
		red=$(_getRed $color)
		green=$(_getGreen $color)
		blue=$(_getBlue $color)

		redDecimal=$(_getDecimal $red)
		greenDecimal=$(_getDecimal $green)
		blueDecimal=$(_getDecimal $blue)

		total=$(_getTotal $redDecimal $greenDecimal $blueDecimal)
		totalWZeros=$(_padNumber $total)

		## Essa é a função de ordenar em um arquivo parte 1
		if [ "$separate" -eq 0 ]
		then
			echo "$totalWZeros:$color" >> "$tempFile"
		else

			yellowRatio=$(_getRatio $redDecimal $greenDecimal)
			cyanRatio=$(_getRatio $blueDecimal $greenDecimal)
			magentaRatio=$(_getRatio $redDecimal $blueDecimal)

			doneColor=0
			[ $redDecimal -le $threshold -a $greenDecimal -le $threshold -a $blueDecimal -le $threshold ] && echo "$totalWZeros:$color" >> "$blackFile" && doneColor=1
			[ $doneColor -eq 0 -a $magentaRatio -le $(($threshold / 4)) -a $yellowRatio -le $(($threshold / 4)) -a $cyanRatio -le $(($threshold / 4)) ] && echo "$totalWZeros:$color" >> "$grayFile" && doneColor=1 
			[ $doneColor -eq 0 -a $magentaRatio -le $threshold -a $greenDecimal -le $(($threshold * 3 / 2)) ] && echo "$totalWZeros:$color" >> "$mFile" && doneColor=1 
			[ $doneColor -eq 0 -a $cyanRatio -le $threshold -a $redDecimal -le $(($threshold * 3 / 2)) ] && echo "$totalWZeros:$color" >> "$cFile" && doneColor=1 
			[ $doneColor -eq 0 -a $yellowRatio -le $threshold -a $blueDecimal -le $(($threshold * 3 / 2)) ] && echo "$totalWZeros:$color" >> "$yFile" && doneColor=1 
			[ $doneColor -eq 0 -a $redDecimal -ge $blueDecimal -a $redDecimal -ge $greenDecimal ] && echo "$totalWZeros:$color" >> "$rFile" && doneColor=1 
			[ $doneColor -eq 0 -a $greenDecimal -ge $redDecimal -a $greenDecimal -ge $blueDecimal ] && echo "$totalWZeros:$color" >> "$gFile" && doneColor=1 
			[ $doneColor -eq 0 -a $blueDecimal -ge $redDecimal -a $blueDecimal -ge $greenDecimal ] && echo "$totalWZeros:$color" >> "$bFile" 
		fi

		## Essa é a função de informar sobre as cores
		if [ "$showInfo" -eq 1 ]
		then
			echo "color: $color -> $total"
			echo "  red: $red -> $redDecimal"
			echo "  green: $green -> $greenDecimal"
			echo "  blue: $blue -> $blueDecimal"
		fi

		if [ $i -eq $maxColors ]; then
			break
		fi

		((i++))

	done < "$colorList"

	## Essa é a função de ordenar em um arquivo parte 2
	if [ "$separate" -eq 0 ]
	then
		cat "$tempFile" | sort | uniq | cut -f 2 -d : > "$outFile"
		rm -rf "$tempFile"
	else
		rm -f "$outFile" 2> /dev/null
		echo "BLACK" >> "$outFile"
		cat "$blackFile" | sort | uniq | cut -f 2 -d : >> "$outFile"
		echo "GRAY" >> "$outFile"
		cat "$grayFile" | sort | uniq | cut -f 2 -d : >> "$outFile"
		echo "RED" >> "$outFile"
		cat "$rFile" | sort | uniq | cut -f 2 -d : >> "$outFile"
		echo "YELLOW" >> "$outFile"
		cat "$yFile" | sort | uniq | cut -f 2 -d : >> "$outFile"
		echo "GREEN" >> "$outFile"
		cat "$gFile" | sort | uniq | cut -f 2 -d : >> "$outFile"
		echo "CYAN" >> "$outFile"
		cat "$cFile" | sort | uniq | cut -f 2 -d : >> "$outFile"
		echo "BLUE" >> "$outFile"
		cat "$bFile" | sort | uniq | cut -f 2 -d : >> "$outFile"
		echo "MAGENTA" >> "$outFile"
		cat "$mFile" | sort | uniq | cut -f 2 -d : >> "$outFile"
		rm -rf "$bFile" "$gFile" "$rFile" "$grayFile" "$yFile" "$mFile" "$cFile" "$blackFile"
	fi

fi

echo "Criando visualização"
i=1
while read color
do
	## Essa é a função de fazer html parte 1
	echo "<div>" >> "$content"
	echo "  <p>$color</p>" >> "$content"
	echo "  <div class='color' style='background-color: $color' ></div>" >> "$content"
	echo "</div>" >> "$content"
	if [ $i -eq $maxColors ]; then
		break
	fi

	((i++))

done < "$fileToHtml"

## Essa é a função de fazer html parte 2
cat "$html1" > "$outHtml"
cat "$html2" >> "$outHtml"
cat "$content" >> "$outHtml"
cat "$html3" >> "$outHtml"
rm -rf $content 

