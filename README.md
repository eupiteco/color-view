# Color Sort: o ordenador mágico

Pega uma lista de cores e ordena por valor (mais claro -> mais escuro) somando o valor de cada canal (R G B). Gera um arquivo html pra poder visualizar melhor os resultados.

## Como usar
```shelscript
	./color-sort.sh [arquivo]
```

O repositório já vem com um arquivo padrão pra exemplo e pra testes, divirta-se

## A fazer

* Documentação e mensagem de erro
* Regras customizadas para cada cor (CMY)
